import Counter from "./Counter";

const CounterGroup = (props) => {
    const {counterList, setCounterList} = props;
    const updateCounterValue = (newValue, indexUpdated) => {
        const counterListUpdated = counterList.map((oldValue, index) => {
            if (indexUpdated === index)
                return newValue;
            else
                return oldValue;
        });
        setCounterList(counterListUpdated);
    }
    return (
        <div>
            {counterList.map((counter, index) => <Counter key = {index}
                                                          index = {index}
                                                          counter = {counter}
                                                          updateCounterValue = {updateCounterValue}></Counter>)}
        </div>
    );
}

export default CounterGroup;