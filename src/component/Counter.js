const Counter = (props) => {
    const {counter, updateCounterValue, index} = props;
    const increase = (e) => {
        console.log(counter, index);
        updateCounterValue(counter + 1, index);
    };
    const decrease = (e) => {
        console.log(counter, index);
        updateCounterValue(counter - 1, index);
    };
    return (
        <div className = "counter">
            <button onClick = {increase}>+</button>
            <h3>{counter}</h3>
            <button onClick = {decrease}>-</button>
        </div>
    );
};

export default Counter;