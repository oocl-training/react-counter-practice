const CounterSizeGenerator = (props) => {
    const {size, setSize} = props;
    const updateSize = (event) => {
        setSize(event.target.value);
        console.log(size);
    }
    return (
        <div>
            Size: <input type = 'number' value = {size || ''} onChange = {updateSize}/>
        </div>
    )
}

export default CounterSizeGenerator;