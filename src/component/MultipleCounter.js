import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroup from "./CounterGroup";
import {useState} from "react";
import CounterGroupSum from "./CounterGroupSum";

const MultipleCounter = () => {
    const [counterList, setCounterList] = useState([]);
    const setSize = (size) => {
        const counters = size ? Array.from({length: size}).fill(0) : [];
        setCounterList(counters);
    };

    const sum = counterList.reduce((sum, currentValue) => sum + currentValue, 0);

    return (<div>
        <CounterSizeGenerator size = {counterList.length}
                              setSize = {setSize}></CounterSizeGenerator>
        <CounterGroupSum sum = {sum}></CounterGroupSum>
        <CounterGroup counterList = {counterList}
                      setCounterList = {setCounterList}></CounterGroup>
    </div>);
}

export default MultipleCounter;